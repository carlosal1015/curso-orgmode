# uso de print

# esto es una variable
numero=3
# esto es la acción de mostrar la variable 
print(numero)
# esto muestra la variable, pero con un texto que explica que esto
print("Esto es un número", numero)

# Booleanos

A=True
B=False
C=True

print(A, type(A))
print(B, type(B))

# Operación AND

print("A and B =",A and B)
print("A and C =",A and C)
print("C and B =",C and B)

# Operación OR

print("A or B =",A or B)
print("A or C =",A or C)
print("C or B =",C or B)

# Tipo op

# Operación OR

print("A or B =",A or B, type(A and B))
print("C and B =",C and B, type(C and B))
