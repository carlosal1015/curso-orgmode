set term png
set output "asistencia.png"
set datafile separator ","
set encoding utf8
set xlabel "dia"
set ylabel "cantidad de personas"
plot "./datos.csv" using 0:2:xticlabels(1) with lines title "Asistencia"